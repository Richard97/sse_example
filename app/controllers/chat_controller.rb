require "redis"

class ChatController < ApplicationController
  include ActionController::Live

  def index
    if params[:username] && !params[:username].empty?
      user = User.find_by_name(params[:username]);

      if user.nil?
        user = User.create({
          name: params[:username]
        })
      end

      messages = Message.all.order('created_at ASC')
    end

    render locals: {
      user: user || nil,
      messages: messages || nil
    }
  end

  def send_message
    if (params[:content] && !params[:content].empty?)
      user = User.find(params[:user])

      user.messages << Message.create({
        content: params[:content]
      })
    end
  end

  def update
    response.headers['Content-Type'] = 'text/event-stream'
    sse = SSE.new(response.stream)

    user = User.find(params[:user]);

    # Message.on_change do |event, id|
    #   if event == 'messages_destroyed'
    #     sse.write '', event: 'reset'
    #   else
    #     message = Message.find(id)
    #     sse.write({
    #       html: render_to_string(partial: '/chat/message', locals: { message: message, user: user }),
    #       content: message.content,
    #       user: message.user.name,
    #       current_user: user.name
    #     })
    #   end
    # end

    redis = Redis.new
    redis.subscribe ['new_message', 'message_reset', 'heartbeat'] do |on|
      on.message do |channel, data|
        case channel
          when 'new_message'
            message = Message.find(data)
            sse.write ({
              html: render_to_string(partial: '/chat/message', locals: { message: message, user: user }),
              content: message.content,
              user: message.user.name,
              current_user: user.name
            })
          when 'message_reset'
            sse.write '', event: channel
          when 'heartbeat'
            sse.write '', event: 'hb'
        end
      end
    end

    render body: nil
  rescue ClientDisconnected
    # client disconnected
  ensure
    redis.quit
    sse.close if sse
  end

  def reset
    Message.destroy_all

    head :no_content
  end

end
