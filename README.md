# Server Sent Events Demo

- create new postgresql database 'sse-test_db' and 'sse-test_test'
- create new postgresql user 'sse-test_dba' with granted access to above databases
- run
		
		$ bundle install
		$ bundle exec rake db:migrate